
public class Buch {

private String titel;
private String autor;
private String isbn;
public Buch (String titel, String autor , String isbn) {
	this.autor=autor;
	this.isbn=isbn;
	this.titel=titel;
	
}
public String getTitel() {
	return titel;
}
public void setTitel(String titel) {
	this.titel=titel;
	
}
public String getAutor() {
	return autor;
}
public void setAutor(String autor) {
	this.autor=autor;
}
public String getIsbn() {
	return isbn;
}
public void setIsbn(String isbn) {
	this.isbn=isbn;
	
}
public String toString() {
	return "Buch [autor=" + this.autor + ", titel=" + this.titel + ", isbn=" + this.isbn + "]";
}
public int compareTo(Buch aBuch) {
	return this.isbn.compareTo(aBuch.getIsbn());
}

	

}
