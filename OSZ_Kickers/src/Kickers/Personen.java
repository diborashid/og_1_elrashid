package Kickers;

public class Personen {
  
private String name;
private int telefonnr;
private boolean istBezahlt;


public Personen() {}
public Personen(String name,int telefonnr,boolean istBezahlt) {
	this.name=name;
	this.telefonnr=telefonnr;
	this.istBezahlt= istBezahlt;
			
		
}
public String getName () {
	return name;
}
public void setName(String name) {
	this.name=name;
}
public int getTelefonnr() {
	return telefonnr;
}
public void setTelefonnr(int telefonnr) {
	this.telefonnr=telefonnr;
}
public boolean getIstbezahlt() {
	return istBezahlt;
}
public void setIstBezahlt(boolean istBezahlt) {
	this.istBezahlt=istBezahlt;
}
}
