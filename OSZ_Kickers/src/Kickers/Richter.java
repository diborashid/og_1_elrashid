package Kickers;

public class Richter extends Personen {
	private int anzahlDerPf;
	
	
	public Richter() {}
	public Richter(String name,int telefonnr,boolean istBezahlt,int anzahlDerPf) {
		super(name, telefonnr , istBezahlt);
		this.anzahlDerPf=anzahlDerPf;
	}
	
	public int getAnzahlDerPf() {
		return anzahlDerPf;
		
	}
	public void setAnzahlDerPf(int anzahlDerPf) {
		this.anzahlDerPf=anzahlDerPf;
	}

}
