package Kickers;

public class Trainer extends Personen {
	private char lizenzklasse;
	private String aufwantsentschaedigung;
	
	public Trainer() {}
	public Trainer(String name,int telefonnr,boolean istBezahlt,char lizenzklasse, String aufwantsentschaedigung) {
		super(name, telefonnr , istBezahlt);
		this.lizenzklasse=lizenzklasse;
		this.aufwantsentschaedigung=aufwantsentschaedigung;
		
	}
	public char getLizenzklasse() {
		return lizenzklasse;
	}
	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse=lizenzklasse;
	}
	public String getAufwantsentschaedigung() {
		return aufwantsentschaedigung;
	}
	public void setAufwantsentschaedigung(String aufwantsentschaedigung) {
		this.aufwantsentschaedigung=aufwantsentschaedigung;
	}

}
