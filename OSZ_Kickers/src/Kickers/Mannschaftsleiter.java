package Kickers;

public class Mannschaftsleiter extends Spieler {
	
	
	
 private String nameDerMannschaft;
 private int rabatt;
 
 
 
 public Mannschaftsleiter() {}
 public Mannschaftsleiter(String name,int telefonnr,boolean istBezahlt,int trikotnr,
		 String spielposition,String nameDerMannschaft, int rabatt) {
	 super(name, telefonnr , istBezahlt,trikotnr,spielposition);
	 this.nameDerMannschaft=nameDerMannschaft;
	 this.rabatt=rabatt;
 }
 
 public String getNameDerMannschaft() {
	 return nameDerMannschaft;
 }
 public void setNameDerMannschaft(String nameDerMannschaft) {
	 this.nameDerMannschaft=nameDerMannschaft;
 }
 public int getRabatt() {
	 return rabatt;
 }
 public void setRabatt(int rabatt) {
	 this.rabatt=rabatt;
 }
}
