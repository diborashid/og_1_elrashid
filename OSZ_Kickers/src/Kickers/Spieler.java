package Kickers;


public class Spieler extends Personen  {
	
	private int trikotnr ;
	private String spielposition;
	
	
	public Spieler() {}
	public Spieler(String name,int telefonnr,boolean istBezahlt,int trikotnr, String spielposition) {
		super(name, telefonnr , istBezahlt);
		this.spielposition=spielposition;
		this.trikotnr=trikotnr;
		
	}
	
	
public int getTrikotnr () {
	return trikotnr;
}
public void  setTrikotnrint (int trikotnr) {
	this.trikotnr=trikotnr;
}
public String getSpielposition() {
	return spielposition;
}
public void setSpielposition(String spielposition) {
	this.spielposition=spielposition;
}
}
