package Primzahl;

public class Primzahl {

	public boolean isPrimzahl(long zahl) {
		if(zahl <2) {
			return false;
		}
		if(zahl==2) {
			return true;
		}
		for(long i=zahl-1;i>1;i--) {
			if(zahl%i==0) {
				return false;
			}
		}
		return true;
	}
}
