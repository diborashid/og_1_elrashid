package og_1_elrashid;

public class Addon {
	
	private int iD_Nummer;
	private String bezeichnung;
	private int verkaufpreis;
	private int bestand;
	private int maximalerBestand;
	
	public Addon() {
		
	}
	public Addon(int ID_Nummer, String bezeichnung, int verkaufspreis, int bestand, int maximalerBestand) {
	}
	public int  getID_Nummer() {
		return iD_Nummer;
	}
	public void setID_Nummer(int iD_Nummer) {
		this.iD_Nummer=iD_Nummer;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getVerkaufpreis() {
		return verkaufpreis;
		
	}
	public void setVerkaufpreis(int verkaufspreis) {
		this.verkaufpreis = verkaufspreis;
	}
	public int getBestand() {
		return this.bestand;
	}
	public void setBestand(int bestand) {
		this.bestand = bestand;
	}
	public int getMax() {
		return maximalerBestand;
	}
	public void setMax(int maximalerBestand) {
		this.maximalerBestand=maximalerBestand;
	}
	public void setVerbrauchen(int verbrauchen) {
		this.bestand = verbrauchen;
	}
	
	public double getGesammtwert() {
	    return this.verkaufpreis * this.bestand;
	}
	

}
